# Blackjack

Implementação do jogo Blackjack em HTML5 utilizando Materialize.

Autor: Lucas Vieira

As informações sobre este projeto estão no arquivo [README.org](README.org).

O código deste projeto (excluindo o framework Materialize, a biblioteca jQuery
e os arquivos de imagem) é distribuido sob a licença MIT. Veja [LICENSE](LICENSE)
para mais detalhes.
